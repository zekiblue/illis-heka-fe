import React from 'react';
import { Row, Col, Card, Statistic} from 'antd';

import { ArrowUpOutlined, UserOutlined } from '@ant-design/icons';

const UserWidget = ({ ...props}) => {
    return (
        <Row className="user-widgets" gutter = {[0,0]} justify="space-between">
            <Col span={7}>
              <Card>
                <Statistic
                  title="Registered Total Users"
                  value={props.registered}
                  prefix={<UserOutlined />}
                    />
              </Card> 
              </Col>
              <Col span={7}>
              <Card>
                <Statistic
                  title="New members this month"
                  value={props.new}
                  prefix={<ArrowUpOutlined />}
                    />
              </Card> 
              </Col>
              <Col span={7}>
              <Card>
                <Statistic
                  title="Tenant Users"
                  value={props.tenant}
                  prefix={<UserOutlined />}
                    />
              </Card>
            </Col>
        </Row>
    )
}

export default UserWidget;
