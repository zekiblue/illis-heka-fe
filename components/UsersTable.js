import React from 'react';
import { Table, Tag, Space, Row, Popconfirm, Button, Col } from 'antd';

const { Column, ColumnGroup } = Table;

const UsersTable = ({ data, ...props}) => {
    return (
        <>
        <Row justify="end" style={{marginBottom: 5, marginTop: 1}}>
            <Col>
                <Space>
                  <Button type="primary">Add User</Button>
                  <Popconfirm title="Are you sure delete this task?" okText="Yes" cancelText="No">
                    <Button type="secondary">Confirm</Button>
                  </Popconfirm>
                </Space>
            </Col>
          </Row>
        <Table dataSource={data}>
            <ColumnGroup title="Name">
                <Column title="First Name" dataIndex="firstName" key="firstName" />
                <Column title="Last Name" dataIndex="lastName" key="lastName" />
            </ColumnGroup>
            <Column title="Age" dataIndex="age" key="age" />
            <Column title="Address" dataIndex="address" key="address" />
            <Column
                title="Tags"
                dataIndex="tags"
                key="tags"
                render={tags => (
                    <>
                    {tags.map(tag => (
                        <Tag color="blue" key={tag}>
                            {tag}
                        </Tag>
                    ))}
                </>
            )}
            />
            <Column
            title="Action"
            key="action"
            render={(text, record) => (
                <Space size="middle">
                <a>Invite {record.lastName}</a>
                <a>Delete</a>
                </Space>
            )}
            />
        </Table>
        </>
    )
}

export default UsersTable;
