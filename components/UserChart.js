import { Chart } from '@antv/g2';
import React from 'react'

const UserChart = ({ data }) => {
    chart = new Chart({
        container: 'container',
        autoFit: true,
        height: 500,
      });
      
      chart.data(data);
      chart.scale('sales', {
        nice: true,
      });
      
      chart.tooltip({
        showMarkers: false,
      });
      chart.interaction('active-region');
      
      chart
        .interval()
        .position('year*sales')
        .style({ radius: [20, 20, 0, 0] });
      
    return (
        chart.render()
    )
}

export default UserChart
