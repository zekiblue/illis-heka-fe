import React from 'react'
import Link from 'next/link';

const dummyLink = ({ children, ...props}) => {
    return (
        <Link as="/" href={props.href}>{ children }</Link>
    )
}

export default dummyLink;
