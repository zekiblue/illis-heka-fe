import UsersTable from "../../components/UsersTable";
import '../../styles/pages/user.css';
import { UserData } from "../../fake_data/UserData";
import UserWidget from "../../components/UserWidget";

export default function All() {
  const datas = UserData;

  return (
        <>          
          <UserWidget registered={10} new={4} tenant={5} />  
          <UsersTable data={datas} className="user-data-"></UsersTable>
        </>
    )
  }