import UsersTable from "../../components/UsersTable";
import '../../styles/pages/user.css';
import { UserDataChart, UserDataOwner } from "../../fake_data/UserData";
import UserWidget from "../../components/UserWidget";
import UserChart from "../../components/UserChart";

export default function Owner() {
  const datas = UserDataOwner;
  const chartdata = UserDataChart;
    return (
           <>
          <UserChart data={chartdata} />          
          <UserWidget registered={100} new={5} tenant={10} />  
          <UsersTable data={datas} className="user-data-"></UsersTable>
        </>
    )
  }