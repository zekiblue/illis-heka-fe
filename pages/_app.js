import '../styles/globals.css'
import "../styles/antd-custom.less";
import General from '../layout/General';

function MyApp({ Component, pageProps }) {
  return (
  <General>
    <Component {...pageProps} />
  </General>
  )
}

export default MyApp
