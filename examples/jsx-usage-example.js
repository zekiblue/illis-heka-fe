<Main>
<div className="pageContainer">
  <p>Contact your account manager to view this page.</p>
</div>
<style jsx>
  {`
    .pageContainer {
      padding: 32px;
    }
  `}
</style>
</Main>