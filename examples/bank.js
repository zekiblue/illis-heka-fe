import './bank.css';

import { Popover } from 'antd'

const BankInfoIcon = () => {
  const content = <div>It is safe to share this information. Entering your sort code and account number will be used to get more loan offers. It will not be used for any other purpose.</div>

  return (
    <Popover placement="top" content={content} trigger="click">
      <div className='red-div'>cool</div>
    </Popover>
  )
}

export default BankInfoIcon;