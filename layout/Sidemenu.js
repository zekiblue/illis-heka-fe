import React from 'react'
import {useState} from 'react';
import {
    DesktopOutlined,
    PieChartOutlined,
    FileOutlined,
    TeamOutlined,
    UserOutlined,
  } from '@ant-design/icons';

import { Menu, Layout } from 'antd';
import Link from 'next/link';
import '../styles/layout/sidemenu.css';
// import Link from '../../components';
// import styles from '../../styles/layout.module.css';


const Sidemenu = () => {
    const { Sider } = Layout;
    const { SubMenu } = Menu;
    const [collapsed, setcollapsed] = useState(false);
    const userTitle = "Users";

    return (
        <Sider collapsible collapsed={collapsed} onCollapse={setcollapsed} >
          <div className="logo"/>
          <Menu theme="dark" defaultSelectedKeys={['dashboard']} mode="inline" className="all-side-menu">
            <Menu.Item key="dashboard" icon={<PieChartOutlined />} >
              <Link href="/">Dashboard</Link>
            </Menu.Item>
            <SubMenu key="user-sub" icon={<UserOutlined />} title={userTitle}>
              <Menu.Item key="user-all"><Link href="/users/all/">All</Link></Menu.Item>
              <Menu.Item key="user-owner"><Link href="/users/owner/">Owner</Link></Menu.Item>
              <Menu.Item key="user-tenant">Tenant</Menu.Item>
              <Menu.Item key="user-worker">Worker</Menu.Item>
            </SubMenu>
            <SubMenu key="property-sub" icon={<TeamOutlined />} title="Property">
              <Menu.Item key="property-all">All</Menu.Item>
              <Menu.Item key="property-habitat">Habitat</Menu.Item>
              <Menu.Item key="property-commercial">Commercial</Menu.Item>
              <Menu.Item key="property-perks">Perks</Menu.Item>
            </SubMenu>
            <SubMenu key="financial-sub" icon={<TeamOutlined />} title="Financial">
              <Menu.Item key="financial-all">All</Menu.Item>
              <Menu.Item key="financial-habitat">Inbound</Menu.Item>
              <Menu.Item key="financial-commercial">Outbound</Menu.Item>
              <Menu.Item key="financial-perks">Recurring</Menu.Item>
            </SubMenu>
            <Menu.Item key="9" icon={<FileOutlined />}>
              Files
            </Menu.Item>
            <Menu.Item key="" icon={<DesktopOutlined />}>
            <Link href="/settings/">
              Settings
            </Link>
            </Menu.Item>
          </Menu>
        </Sider>
    )
}

export default Sidemenu
