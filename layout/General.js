import React from 'react'
import { Layout } from 'antd';
import Sidemenu from './Sidemenu';
import Footer from './Footer';
import Header from './Header';



const General = ({ children }) => {
    const { Content } = Layout;
    return (
      <Layout style={{ minHeight: '100vh' }} hasSider>
        <Sidemenu/>
        <Layout>
          <Header/>
          <Content style={{ margin: '0 16px' }}>
            <div style={{ padding: 24, minHeight: 360}}>
                { children }
            </div>
          </Content>
          <Footer/>
        </Layout>
      </Layout>
    )
}

export default General;